window.$$ = window.$$ || {};

// import '../../bootstrap/dist/css/bootstrap.css';
// import '../../bootstrap/dist/js/bootstrap.min.js';
import '../../src/vendor/jquery-ui.js';
import '../../src/vendor/chosen.jquery.min.js';
import '../../src/vendor/chosen.css';

import 'less/fonts.less';
import 'less/icons.less';
import 'less/global.less';
import 'less/forms.less';
import 'less/page.less';
import 'less/blocks.less';

import 'jquery.inputmask';

import MenuSlide from './Components/MenuSlide.es6';
import ContentSlider from './Components/ContentSlider.es6';
import Popup from './Components/Popup.es6';

class Application {
    constructor() {
        this._initMenuSlide();
        this._initContentSlider();
        this._initPopup();
    }

    _initMenuSlide() {
        $('.js-menu-slide').each(function() {
            new MenuSlide($(this));
        });
    }

    _initPopup() {
        $('.js-popup').each(function() {
            new Popup($(this));
        });
    }

    _initContentSlider() {
        $('.js-content-slider').each(function() {
            new ContentSlider($(this));
        });
    }
}

$(function () {
    $$.window = $(window);
    $$.body = $('body');
    $$.html = $('html');

    $$.windowWidth = $$.window.width();
    $$.windowHeight = $$.window.height();

    $$.window.on('resize', () => {
        $$.windowWidth = $$.window.width();
        $$.windowHeight = $$.window.height();
    });

    $$.application = new Application();

    /**
     * Определение включен ли режим редактирования Bitrix
     */
    // if (location.href.indexOf('bitrix_include_areas=Y') > -1) {
    //     $('.js-main-slider').css({paddingTop: 50});
    // }
});
