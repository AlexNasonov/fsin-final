export default class ContentSlider {
    constructor(root, options = {}) {
        const defaultOptions = {
            screenCount: 1,
        };

        this.root = root;
        this.options = _.assign(defaultOptions, options);
        this.currentIndex = 0;

        this._cacheNodes();
        this._initialize();
        this._bindEvents();
    }

    _cacheNodes() {
        this.nodes = {
            slides: this.root.find('.js-content-slide'),
            bulls: this.root.find('.js-content-menu')
        };
    }

    _initialize () {
        this.itemsCount = this.nodes.slides.length;
        this.nodes.slides.hide().eq(this.currentIndex).show();
    }

    _bindEvents() {
        if (this.nodes.bulls.length) {
            this.nodes.bulls.on('click', (event) => {
                this._goTo($(event.currentTarget).index());
            });
        }
    }

    _goTo(index) {
        if (this.currentIndex !== (index)) {
            this.nodes.slides.eq(this.currentIndex).fadeOut(0);
            this.nodes.slides.eq(index).fadeIn(1000);

            if (this.nodes.bulls.length) {
                this.nodes.bulls.eq(this.currentIndex).removeClass('active');
            }

            this.currentIndex = index;

            if (this.nodes.bulls.length) {
                this.nodes.bulls.eq(this.currentIndex).addClass('active');
            }
        }
    }
}
