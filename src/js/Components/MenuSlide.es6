export default class MenuSlide {
    constructor(root, options = {}) {
        const defaultOptions = {};

        this.root = root;
        this.options = _.assign(defaultOptions, options);
        this._cacheNodes();
        this._bindEvents();
    }

    _cacheNodes() {
        this.nodes = {
            menu: this.root.find('.js-menu'),
            content: this.root.find('.js-content')
        };
    }

    _bindEvents() {
            this.nodes.menu.on('click', (event) => {
                this._openMenu($(event.currentTarget));
            });
    }

    _openMenu() {
        if (this.nodes.content.hasClass('active')) {
            this.nodes.content.fadeOut(200);
            this.nodes.content.removeClass('active');
        } else {
            this.nodes.content.fadeIn(500);
            this.nodes.content.addClass('active');
        }
    }
}
