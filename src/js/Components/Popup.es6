export default class Popup {
    constructor(root, options = {}) {
        const defaultOptions = {
            content: null,
            onCreate: _.noop()
        };

        this.root = root;
        this.options = _.assign(defaultOptions, options);

        this.classes = this.root.data('classes');

        this._cacheNodes();
        this._bindEvents();
        this._ready();
    }

    _cacheNodes() {
        this.nodes = {
            togglers: $(`.js-popup-open[data-target="${ this.root.attr('id')}"]`)
        };
    }

    _bindEvents() {
        this.nodes.togglers.on('click', () => {
            this.open();
            return false;
        });

        $(document).keyup((event) => {
            if (event.keyCode === 27) {
                this._hidePopup();
            }
        });
    }

    _ready() {
        this._createPopup(this.options.content ? this.options.content : this.root);
    }

    open() {
        if (!this.nodes.popup) {
            this._createPopup(this.options.content ? this.options.content : this.root);
        }

        this._showPopup();
    }

    close() {
        if (this.nodes.popup) {
            this._hidePopup();
        }
    }

    _showPopup() {
        this.nodes.popup.fadeIn();
        this.isSecondPopup = $$.html.hasClass('popup');
        $$.html.addClass('popup');
    }

    _hidePopup() {
        if (this.nodes.popup) {
            this.nodes.popup.fadeOut(() => {
                if (!this.isSecondPopup) {
                    $$.html.removeClass('popup');
                }
            });
        }
    }

    _createPopup(content) {
        this.nodes.popup = $(`
		<div class="g-popup">
		    <div class="overlay"></div>
                <div class="content-wrapper">
                <div class="js-content">
                </div>
            </div>
		</div>`);

        this.nodes.popup.data('controller', this);

        this.nodes.content = this.nodes.popup.find('.js-content');

        $$.body.append(this.nodes.popup);
        this.nodes.content.append(content);
        this.nodes.close = this.nodes.popup.find('.js-close');

        this.nodes.secondTogglers = this.nodes.content.find('.js-popup-open');
        this.nodes.secondTogglers.on('click', () => {
            this.isSecondPopup = true;
            this._hidePopup();
        });
        this.nodes.popup.hide();

        this.nodes.close.on('click', () => {
            $$.html.removeClass('popup');
            this._hidePopup();
        });

        if (typeof this.options.onCreate === 'function') {
            this.options.onCreate(this.nodes.content);
        }

        this.nodes.popup.on('click', (event) => {
            if (event.target !== this.nodes.popup.get(0)) {
                return;
            }

            this._hidePopup();
        });
    }
}
