<?php

if (rand(0, 1)) {
	$response = array(
		'successHTML' => 'Все ок'
	);
} else {
	$response = array(
		'validationErrors' => array(
			'base' => array('Попробуйте ввести другие данные'),
			'email' => array('Неправильное имя')
		)
	);
}

echo json_encode($response);
