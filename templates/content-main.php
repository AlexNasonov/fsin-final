<div class="wrapper-input">
    <div class="field">
        <input type="search" class="input-search" placeholder="Поиск заведения по названию и городу">
        <span class="span-arrow">
            <span class="arrow">
                <img class="arrow" src=".././src/images/sprite_svg.svg #arrow">
            </span>
        </span>
    </div>
</div>
<div class="main-background">
    <div class="wrapper-content">
        <div class="content-main-caption">
            Перевод денег осужденным - помощь людям в тюрьмах
        </div>
        <div class="wrapper-content-text">
            <div class="content-main-text">
                Теперь есть удобный способ помочь осужденному. Родственники и друзья могут сделать перевод и отправить деньги в колонию, следственный изолятор и другие учреждения ФСИН в любое время. Деньги будут зачислены на лицевой счет заключенного, и он сможет совершенно законно купить все необходимое в магазине исправительного учреждения. Это лучше и быстрее, чем передачка или посылка.
            </div>
        </div>
    </div>
    <div class="flex-wrap">
        <div class="top-menu-container">
            <div class="column-top">
                <div class="item">
                    <img class="shield" src=".././src/images/sprite_svg.svg #shield">
                    <div class="d"></div>
                    <div class="item-title shield-title">
                        Банк сделает денежный перевод на расчетный счет ФСИН в Управление Федерального Казначейства
                    </div>
                </div>
                <div class="item">
                    <img class="blank" src=".././src/images/sprite_svg.svg #blank2">
                    <div class="item-title">
                        Безопасно оплатите перевод картой, электронными деньгами или с баланса телефона
                    </div>
                </div>
            </div>
            <div class="column-down">
                <div class="item">
                    <img class="blank" src=".././src/images/sprite_svg.svg #blank">
                    <div class="item-title">
                        Бухгалтерия учреждения зачислит деньги осужденному на лицевой счет
                    </div>
                </div>
                <div class="item">
                    <img class="blank" src=".././src/images/sprite_svg.svg #blank3">
                    <div class="item-title">
                        Cо своего лицевого счета осужденный купит продукты в магазине учреждения
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-for-map">
    <div class="dist-main-caption">
        Выберите учреждение ФСИН России
    </div>
    <div class="map-container">
        <a href="#" class="wrapper-box">
            <div class="box">
                <div class="dist-image">
                    <div class="dist-image-number">1</div>
                </div>
                <div class="box-region-name">Белгородская область</div>
            </div>
        </a>
        <a href="#" class="wrapper-box">
            <div class="box">
                <div class="dist-image">
                    <div class="dist-image-number">2</div>
                </div>
                <div class="box-region-name">Белгородская область</div>
            </div>
        </a>
        <a href="#" class="wrapper-box">
            <div class="box">
                <div class="dist-image">
                    <div class="dist-image-number">3</div>
                </div>
                <div class="box-region-name">Белгородская область</div>
            </div>
        </a>
        <a href="#" class="wrapper-box">
            <div class="box">
                <div class="dist-image">
                    <div class="dist-image-number">1</div>
                </div>
                <div class="box-region-name">Белгородская область</div>
            </div>
        </a>
        <a href="#" class="wrapper-box">
            <div class="box">
                <div class="dist-image">
                    <div class="dist-image-number">1</div>
                </div>
                <div class="box-region-name">Белгородская область</div>
            </div>
        </a>
        <a href="#" class="wrapper-box">
            <div class="box">
                <div class="dist-image">
                    <div class="dist-image-number">1</div>
                </div>
                <div class="box-region-name">Белгородская область</div>
            </div>
        </a>
        <a href="#" class="wrapper-box">
            <div class="box">
                <div class="dist-image">
                    <div class="dist-image-number">1</div>
                </div>
                <div class="box-region-name">Белгородская область</div>
            </div>
        </a>
        <a href="#" class="wrapper-box">
            <div class="box">
                <div class="dist-image">
                    <div class="dist-image-number">1</div>
                </div>
                <div class="box-region-name">Белгородская область</div>
            </div>
        </a>
        <a href="#" class="wrapper-box">
            <div class="box">
                <div class="dist-image">
                    <div class="dist-image-number">1</div>
                </div>
                <div class="box-region-name">Белгородская область</div>
            </div>
        </a>
        <a href="#" class="wrapper-box">
            <div class="box">
                <div class="dist-image">
                    <div class="dist-image-number">1</div>
                </div>
                <div class="box-region-name">Белгородская область</div>
            </div>
        </a>
        <a href="#" class="wrapper-box">
            <div class="box">
                <div class="dist-image">
                    <div class="dist-image-number">1</div>
                </div>
                <div class="box-region-name">Белгородская область</div>
            </div>
        </a>
        <a href="#" class="wrapper-box">
            <div class="box">
                <div class="dist-image">
                    <div class="dist-image-number">1</div>
                </div>
                <div class="box-region-name">Белгородская область</div>
            </div>
        </a>
        <a href="#" class="wrapper-box">
            <div class="box">
                <div class="dist-image">
                    <div class="dist-image-number">1</div>
                </div>
                <div class="box-region-name">Белгородская область</div>
            </div>
        </a>
        <a href="#" class="wrapper-box">
            <div class="box">
                <div class="dist-image">
                    <div class="dist-image-number">1</div>
                </div>
                <div class="box-region-name">Белгородская область</div>
            </div>
        </a>
        <a href="#" class="wrapper-box">
            <div class="box">
                <div class="dist-image">
                    <div class="dist-image-number">1</div>
                </div>
                <div class="box-region-name">Белгородская область</div>
            </div>
        </a>
        <div class="list-wrap">
            <div class="list">
                <a href="#" class="list-link">Предыдущие 15 </a>
            </div>
            <div class="list">
                <a href="#" class="list-link">Следующие 15</a>
            </div>
        </div>
    </div>
    <div class="right-block">
        <div class="fsin">Гарантии безопасности</div>
        <div class="right-block-up">
            <div class="right-block-item">
                <div class="right-block-item-image">
                    <img class="right-image" src=".././src/images/sprite_svg.svg #safebox">
                </div>
                <div class="right-block-item-title">Платежи безопасны. Обмен данными с банком идет по защищенному шифрованному каналу. Мы не храним данные карт.</div>
            </div>
            <div class="right-block-item">
                <div class="right-block-item-image">
                    <img class="right-image" src=".././src/images/sprite_svg.svg #bankmini">
                </div>
                <div class="right-block-item-title">Оператор платежа – РФИ банк. Лицензия Центрального Банка России №3351 от 06.08.2015.</div>
            </div>
        </div>
        <div class="right-block-down">
            <div class="right-block-item">
                <div class="right-block-item-image">
                    <img class="right-image" src=".././src/images/sprite_svg.svg #contact">
                </div>
                <div class="right-block-item-title">Только мы даем подтверждение банка о переводе денег на расчетный счет учреждения ФСИН.</div>
            </div>
            <div class="right-block-item">
                <div class="right-block-item-image">
                    <img class="right-image" src=".././src/images/sprite_svg.svg #contact2">
                </div>
                <div class="right-block-item-title">Ежедневная отправка информации о платежах в бухгалтерию УФСИН для скорейшего зачисления на лицевой счет заключенного.</div>
            </div>
        </div>
    </div>
</div>