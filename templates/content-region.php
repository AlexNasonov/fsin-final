

<div class="wrapper">
    <div class="content-caption">Владимирская область</div>
    <div class="prison-wrapper">
        <div class="prison-list">

            <a href="#" class="prison-block">
                <div class="prison-block-tittle">ИЗ 77/1
                    Матросская Тишина</div>
                <div class="prison-block-sub-tittle">СИЗО</div>
                <div class="prison-block-address">Москва, ул. Матросская Тишина,
                    18а
                </div>
            </a>
            <div class="prison-block">
                <div class="prison-block-tittle">ИЗ 77/1</div>
                <div class="prison-block-sub-tittle">Матросская Тишина</div>
                <div class="prison-block-address">Москва, ул. Матросская Тишина,
                    18а
                </div>
            </div>
            <div class="prison-block">
                <div class="prison-block-tittle">ИЗ 77/1</div>
                <div class="prison-block-sub-tittle">Матросская Тишина</div>
                <div class="prison-block-address">Москва, ул. Матросская Тишина,
                    18а
                </div>
            </div>
            <div class="prison-block">
                <div class="prison-block-tittle">ИЗ 77/1</div>
                <div class="prison-block-sub-tittle">Матросская Тишина</div>
                <div class="prison-block-address">Москва, ул. Матросская Тишина,
                    18а
                </div>
            </div>
            <div class="prison-block">
                <div class="prison-block-tittle">ИЗ 77/1</div>
                <div class="prison-block-sub-tittle">Матросская Тишина</div>
                <div class="prison-block-address">Москва, ул. Матросская Тишина,
                    18а
                </div>
            </div>
            <div class="prison-block">
                <div class="prison-block-tittle">ИЗ 77/1</div>
                <div class="prison-block-sub-tittle">Матросская Тишина</div>
                <div class="prison-block-address">Москва, ул. Матросская Тишина,
                    18а
                </div>
            </div>
            <div class="prison-block">
                <div class="prison-block-tittle">ИЗ 77/1</div>
                <div class="prison-block-sub-tittle">Матросская Тишина</div>
                <div class="prison-block-address">Москва, ул. Матросская Тишина,
                    18а
                </div>
            </div>
            <div class="prison-block">
                <div class="prison-block-tittle">ИЗ 77/1</div>
                <div class="prison-block-sub-tittle">Матросская Тишина</div>
                <div class="prison-block-address">Москва, ул. Матросская Тишина,
                    18а
                </div>
            </div>
            <div class="prison-block-return">Вернуться к округам</div>
        </div>
    </div>


    <div class="right-menu">
        <div class="right-menu-contacts">УФСИН России по Владимирской области</div>
        <div class="right-menu-contacts">Контакты:
            Кудрин Александр Юрьевич
            полковник внутренней службы
            <a href="tel:+7(4212) 70-81-16" class="right-menu-link">7(4212) 70-81-16</a>
             <a href="www.27.fsin.su" class="right-menu-link">www.27.fsin.su</a>
             <a href="mailto:cito_khv@mail.ru" class="right-menu-link">cito_khv@mail.ru</a>
            680038, г. Хабаровск,ул. Яшина, 50</div>
    </div>
</div>

<div class="region-background">
    <div class="wrapper-content">
        <div class="wrapper-content-text">
            <div class="content__main-caption">Перевод денег осужденным - помощь
                людям в тюрьмах
            </div>
            <div class="content__main-text">
                Теперь есть удобный способ помочь осужденному.
                Родственники и друзья могут сделать перевод и отправить деньги в
                колонию, следственный изолятор и другие учреждения ФСИН в любое
                время.
                Деньги будут зачислены на лицевой счет заключенного, и он сможет
                совершенно законно купить все необходимое в магазине
                исправительного
                учреждения. Это лучше и быстрее, чем передачка или посылка
            </div>
        </div>
        <div class="wrapper-images">
            <div class="flex-item-top">
                <div class="flex-item-block">
                    <img class="flex-item-image shield"
                         src=".././src/images/sprite_svg.svg #shield">
                    <div class="shield-title">
                        Безопасно оплатите перевод картой, электронными деньгами
                        или с баланса телефона
                    </div>
                </div>
                <span class="gray-cap"></span>
                <div class="flex-item-block">
                    <img class="flex-item-image"
                         src=".././src/images/sprite_svg.svg #blank2">
                    <div class="flex-item-title">
                        Банк сделает денежный перевод на
                        расчетный счет ФСИН в Управление
                        Федерального Казначейства
                    </div>
                </div>
                <span class="gray-cap"></span>
            </div>
            <div class="flex-item-down">
                <div class="flex-item-block">
                    <img class="flex-item-image"
                         src=".././src/images/sprite_svg.svg #blank">
                    <div class="flex-item-title">
                        Бухгалтерия учреждения зачислит деньги осужденному на лицевой счет
                    </div>
                </div>
                <span class="gray-cap"></span>
                <div class="flex-item-block">
                    <img class="flex-item-image"
                         src=".././src/images/sprite_svg.svg #blank3">
                    <div class="flex-item-title">
                        Со своего лицевого счета осужденный купит продукты в магазине учреждения
                    </div>
                </div>
            </div>
        </div>
        <div class="wrapper-content-text">
            <div class="content__main-caption">
                Как перевести деньги осужденному. Инструкция
            </div>
            <div class="content__main-text">
                Чтобы сделать денежный перевод заключенному или осужденному,
                выберите учреждение Центральный Федеральный Округ с помощью
                поиска или из предложенных вариантов в нужном вам регионе.
                Укажите ФИО получателя полностью, дополнительную информацию,
                например, номер бригады, если знаете, год рождения получателя и
                нажмите «ПЕРЕВЕСТИ». На следующей странице введите данные карты,
                дождитесь получения смс от банка, введите его в соответствующее
                поле, и всё. Денежный перевод в учреждение Центральный
                Федеральный Округ будет отправлен банковским переводом и
                зачислен на лицевой счет осужденного.
            </div>
        </div>
        <div class="wrapper-content-text">
            <div class="content__main-caption">
                Сроки зачисления перевода на лицевой счет осужденного или
                заключенного
            </div>
            <div class="content__main-text">
                Каждое учреждение ФСИН имеет лицевой счет в территориальном
                Управлении Федерального Казначейства России. У осужденных или
                заключенных есть номер лицевого счета в учреждении ФСИН. Это
                внутренняя информация учреждения. Поэтому, денежный перевод
                сначала идет в УФК, потом зачисляется на счет учреждения, и
                только потом зачисляется на лицевой счет осужденного или
                заключенного. По нашей практике, на зачисление перевода на
                лицевой счет заключенного или осужденного требуется 5-7 рабочих
                дней, от момента отправки до момента зачисления.Каждое
                учреждение ФСИН имеет лицевой счет в территориальном Управлении
                Федерального Казначейства России. У осужденных или заключенных
                есть номер лицевого счета в учреждении ФСИН. Это внутренняя
                информация учреждения. Поэтому, денежный перевод сначала идет в
                УФК, потом зачисляется на счет учреждения, и только потом
                зачисляется на лицевой счет осужденного или заключенного. По
                нашей практике, на зачисление перевода на лицевой счет
                заключенного или осужденного требуется 5-7 рабочих дней, от
                момента отправки до момента зачисления.
            </div>
        </div>
    </div>
</div>

