<div class="payment-wrapper">
    <div class="title-button">
        <div class="payment-title">
            Дальневосточный федеральный округ
            УФСИН России по Хабаровскому краю
            ИК-29 Большой Камень
        </div>
        <button class="return-region"><span class="backward-adapt">
                <span class="return-region-link" href="#">
                                    <img
                        class="backward"
                        src=".././src/images/sprite_svg.svg #backward">
                </span>
                </span>Вернуться к округам</button>
    </div>

    <div class="payment-sub-title">
        ФКУ "исправительная колония № 29 главного УФСИН по Приморскому
        краю",
        инн 2503020938
    </div>

    <div class="content-center">
        <div class="payment-forms">
            <div class="forms-container">

                <div class="input-style">
                    <div class="input-name">Номер телефона для связи:</div>
                    <input class="input-form" type="text" required
                           placeholder="+7 (000) 000-00-00">
                </div>

                <div class="input-style">
                    <div class="input-name">ФИО осужденного полностью:</div>
                    <input class="input-form" type="text" required
                           placeholder="Иванов Иван Иванович">
                </div>

                <div class="input-style">
                    <div class="input-name">Дата рождения ДД.ММ.ГГГГ:</div>
                    <input  class="input-form input-datepick" type="text" required
                           placeholder="02. 02.1980">

                    <img class="calendar" src=".././src/images/sprite_svg.svg #calendar">



                </div>

                <div class="input-style">
                    <div class="input-name">Способ оплаты:</div>
                    <input class="input-form" type="text" required
                           placeholder="Банковская карта">
                </div>

                <div class="input-style">
                    <div class="input-name">Сумма:</div>
                    <input class="input-form input-sum" type="text" required
                           placeholder="15 000"> <div class="input-name-sum">От 1 до 15000a
                    Комиссия 4%, мин 40a</div>
                </div>

                <button class="button-translate">Перевести</button>

            </div>
        </div>
        <div class="right-block">
            <div class="right-block-up">
                <div class="right-block-item">
                    <div class="right-block-item-image">
                        <img class="right-image"
                             src=".././src/images/sprite_svg.svg #safebox">
                    </div>
                    <div class="right-block-item-title">Платежи безопасны.
                        Обмен данными с банком идет по защищенному
                        шифрованному каналу. Мы не храним данные карт.
                    </div>
                </div>
                <div class="right-block-item">
                    <div class="right-block-item-image">
                        <img class="right-image"
                             src=".././src/images/sprite_svg.svg #bankmini">
                    </div>
                    <div class="right-block-item-title">Оператор платежа –
                        РФИ банк. Лицензия Центрального Банка России №3351
                        от 06.08.2015.
                    </div>
                </div>
            </div>
            <div class="right-block-down">
                <div class="right-block-item">
                    <div class="right-block-item-image">
                        <img class="right-image"
                             src=".././src/images/sprite_svg.svg #contact">
                    </div>
                    <div class="right-block-item-title">Только мы даем
                        подтверждение банка о переводе денег на расчетный счет
                        учреждения ФСИН.
                    </div>
                </div>
                <div class="right-block-item">
                    <div class="right-block-item-image">
                        <img class="right-image"
                             src=".././src/images/sprite_svg.svg #contact2">
                    </div>
                    <div class="right-block-item-title">Ежедневная отправка
                        информации о платежах в бухгалтерию УФСИН для скорейшего
                        зачисления на лицевой счет заключенного.
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="div">
    <div class="payment-info">
        <div class="payment-info-contacts">
            <div class="payment-info-caption">Контакты</div>
            <span class="payment-info-word">Начальник :</span> Кузнецов Александр Юрьевич

            <span class="payment-info-word"> Адрес:</span> 692802, Приморский край, г. Большой
            Камень, ул. Дзержинского, 1

            <span class="payment-info-word">Эл. почта:</span>  <a href="mailto:ik29pk@yandex.ru" class="right-menu-link">ik29pk@yandex.ru</a>

            <div class="payment-info-caption">Приёмная</div>

            <span class="payment-info-word">Адрес:</span> 692802, Приморский край, г. Большой Камень, ул. Дзержинского, 1
            <span class="payment-info-word">Телефон:</span> +7 (555) 555-13-11

           <span class="payment-info-word"> Прием передач:</span>
            ежедневно 08.30 - 13.00, 14.00 - 15.00

            <span class="payment-info-word">Прием заявлений:</span>
            <span class="payment-info-word">на короткие свидания:</span>
            16.00 - 17.00 По понедельникам
            на длительные свидания:
            16.00 - 17.00 По понедельникам
           <span class="payment-info-word"> Санитарный день:</span> последний день месяца
        </div>
    </div>
        <div class="payment-info">
            <div class="payment-info-contacts">
                <div class="payment-info-block">                <div class="payment-info-caption">Информация о приеме</div>
                <span class="payment-info-word">Иванов Иван Иванович</span>
                должность
                +7 (555) 555-13-11,  +7 (555) 555-13-11
                <a href="mailto:ik29pk@yandex.ru" class="right-menu-link">ik29pk@yandex.ru</a>
                Прием: 16.00 - 17.00 По понедельникам
                </div>

                <div class="payment-info-block">
                <span class="payment-info-word">Иванов Иван Иванович</span>
                должность
                +7 (555) 555-13-11,  +7 (555) 555-13-11
                <a href="mailto:ik29pk@yandex.ru" class="right-menu-link">ik29pk@yandex.ru</a>
                Прием: 16.00 - 17.00 По понедельникам
                </div>

                <div class="payment-info-block">
                <span class="payment-info-word">Иванов Иван Иванович</span>
                должность
                +7 (555) 555-13-11,  +7 (555) 555-13-11
                <a href="mailto:ik29pk@yandex.ru" class="right-menu-link">ik29pk@yandex.ru</a>
                Прием: 16.00 - 17.00 По понедельникам
                </div>

                <div class="payment-info-block">
                <span class="payment-info-word">Иванов Иван Иванович</span>
                должность
                +7 (555) 555-13-11,  +7 (555) 555-13-11
                <a href="mailto:ik29pk@yandex.ru" class="right-menu-link">ik29pk@yandex.ru</a>
                Прием: 16.00 - 17.00 По понедельникам
                </div>

                <div class="payment-info-block">
                <span class="payment-info-word">Иванов Иван Иванович</span>
                должность
                +7 (555) 555-13-11,  +7 (555) 555-13-11
                <a href="mailto:ik29pk@yandex.ru" class="right-menu-link">ik29pk@yandex.ru</a>
                Прием: 16.00 - 17.00 По понедельникам
                </div>

            </div>
        </div>
        <div class="payment-info">
            <div class="payment-info-contacts">
                <div class="payment-info-caption">Перевод денег осужденным
                    </div>
                    <div class="payment-info-text">
                        Теперь есть удобный способ помочь осужденному.
                        Родственники
                        и друзья могут сделать перевод и отправить деньги в
                        колонию,
                        следственный изолятор и другие учреждения ФСИН в любое
                        время. Деньги будут зачислены на лицевой счет
                        заключенного,
                        и он сможет совершенно законно купить все необходимое в
                        магазине исправительного учреждения. Это лучше и
                        быстрее,
                        чем передачка или посылка.
                    </div>

                <div class="wrapper-images">
                    <div class="flex-item-top">
                        <div class="flex-item-block">
                            <img class="flex-item-image shield"
                                 src=".././src/images/sprite_svg.svg #shield">
                            <div class="shield-title">
                                Безопасно оплатите перевод картой, электронными деньгами
                                или с баланса телефона
                            </div>
                        </div>
                        <span class="gray-cap"></span>
                        <div class="flex-item-block">
                            <img class="flex-item-image"
                                 src=".././src/images/sprite_svg.svg #blank2">
                            <div class="flex-item-title">
                                Банк сделает денежный перевод на
                                расчетный счет ФСИН в Управление
                                Федерального Казначейства
                            </div>
                        </div>
                    </div>
                    <div class="flex-item-down">
                        <div class="flex-item-block">
                            <img class="flex-item-image"
                                 src=".././src/images/sprite_svg.svg #blank">
                            <div class="flex-item-title">
                                Бухгалтерия учреждения зачислит деньги осужденному на лицевой счет
                            </div>
                        </div>
                        <div class="flex-item-block">
                            <img class="flex-item-image"
                                 src=".././src/images/sprite_svg.svg #blank3">
                            <div class="flex-item-title">
                                Со своего лицевого счета осужденный купит продукты в магазине учреждения
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>