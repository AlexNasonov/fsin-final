<?
if (isset($_GET['dev'])) {
    $url_prefix = 'http://localhost:8080';
} elseif (isset($_GET['test'])) {
    $url_prefix = '/test';
} else {
    $url_prefix = '' ;
}
?>

<!DOCTYPE html>
<html lang="en">
<head>


    <meta charset="UTF-8">
    <title>Example</title>
<!--    <link rel="stylesheet" type="text/css" href="--><?//= $url_prefix ?><!--/assets/main.css">-->
    <link rel="icon" type="image/png" href="favicon.png" />

    <meta property="og:url" content="http://www.your-domain.com/your-page.html"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="Your Website Title"/>
    <meta property="og:description" content="Your description"/>
    <meta property="og:image" content="http://www.your-domain.com/path/image.jpg"/>
    <link rel="icon" type="image/vnd.microsoft.icon" href="../index.php"/>
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">


</head>
<body>


<script src="<?= $url_prefix ?>/assets/common.js"></script>
<script src="<?= $url_prefix ?>/assets/main.js"></script>

</body>
</html>
