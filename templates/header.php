<div class="wrapper-header">
    <div class="header-content">

        <div class="menu-toggle">
            <img class="menu-toggle-item" src=".././src/images/sprite_svg.svg #menu-toggle">
        </div>

        <div class="header-left-block">
            <div class="fsin-title">#ПОМОГИ.ДЕНЬГАМИ</div>
            <div class="fsin-sub-title">Переводы осужденным ФСИН РФ</div>
        </div>

        <div class="header-link">
            <a href="#" class="header-link-title">Список учреждений</a>
            <a href="#" class="header-link-title">История платежей</a>
            <a href="#" class="header-link-title">Юридическая информация</a>
        </div>

        <div class="lupe-row">
            <img class="lupe" src=".././src/images/sprite_svg.svg #loupe">
        </div>

        <div class="logo-bank">
            <div class="bank-adapt">
                <img class="bank-image" src=".././src/images/sprite_svg.svg #logo-bank">
                <div class="bank-title">Банк оператор переводов</div>
            </div>
        </div>


        <form class="enter-phone">
            <div class="enter-phone-title">Телефон:</div>
            <input class="enter-phone-input" type="text" placeholder="+7 (___) ___ - __ - __"><br>
        </form>

        <form class="phone-code">
            <div class="enter-phone-title">Код:</div>
            <input class="phone-code-input" type="text" placeholder=" ------ "><br>
        </form>

        <button class="button-enter">Войти </button> <div class="cross">&#10006;</div>
        <button class="button-send">Отправить </button>
        <button class="button-confirm">Подтвердить </button>
        <button class="button-exit">Выйти </button>

    </div>
</div>