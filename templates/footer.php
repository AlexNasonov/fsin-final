<div class="footerSM">
    <span class="footerSM-text">Расскажите друзьям :</span>
    <div class="SM-repost-dekstop">
        <script type="text/javascript">
            (function() {
                if (window.pluso)
                    if (typeof window.pluso.start == "function") return;
                if (window.ifpluso == undefined) {
                    window.ifpluso = 1;
                    var d = document,
                        s = d.createElement('script'),
                        g = 'getElementsByTagName';
                    s.type = 'text/javascript';
                    s.charset = 'UTF-8';
                    s.async = true;
                    s.src = ('https:' == window.location.protocol ? 'https' : 'http') + '://share.pluso.ru/pluso-like.js';
                    var h = d[g]('body')[0];
                    h.appendChild(s);
                }
            })();
        </script>
        <div class="pluso" data-background="none;" data-options="big,square,line,horizontal,counter,sepcounter=1,theme=14" data-services="vkontakte,odnoklassniki,facebook,twitter,google,moimir"></div>
    </div>
    <div class="SM-repost-mob">
        <script type="text/javascript">
            (function() {
                if (window.pluso)
                    if (typeof window.pluso.start == "function") return;
                if (window.ifpluso == undefined) {
                    window.ifpluso = 1;
                    var d = document,
                        s = d.createElement('script'),
                        g = 'getElementsByTagName';
                    s.type = 'text/javascript';
                    s.charset = 'UTF-8';
                    s.async = true;
                    s.src = ('https:' == window.location.protocol ? 'https' : 'http') + '://share.pluso.ru/pluso-like.js';
                    var h = d[g]('body')[0];
                    h.appendChild(s);
                }
            })();
        </script>
        <div class="pluso" data-background="none;" data-options="medium,square,line,horizontal,counter,sepcounter=1,theme=14" data-services="vkontakte,odnoklassniki,facebook,twitter,google,moimir"></div>
    </div>
</div>
<div class="footerSM-line"></div>
<div class="wrapper-footer">
    <div class="content">
        <div class="footer">
            <div class="footer-left">
                <a class="footer-right-link" href="#">Список учреждений</a>
                <br>
                <a class="footer-right-link" href="#">История платежей</a>
                <br>
                <a class="footer-right-link" href="#">Юридическая информация</a>
                <br>
            </div>

            <div class="footer-right">
                <div class="footer-right-text">&#169 2017 ООО "#ПОМОГИ.ДЕНЬГАМИ"
                    <br>
                </div>
                <a href="tel:+7 (400) 000-00-00" class="right-menu-link">+7 (400) 000-00-00</a>
                <br>
                <a href="mailto:info@pomogidengami.ru" class="right-menu-link">info@pomogidengami.ru</a>
                <br>
            </div>
        </div>
    </div>
</div>
<script src="<?= $url_prefix ?>/assets/common.js"></script>
<script src="<?= $url_prefix ?>/assets/main.js"></script>
</body>
</html>